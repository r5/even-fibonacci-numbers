module EvenFibonacciNumbers

  def self.included(base)
    base.extend(ClassMethods)
  end

  def fibonacci_sum(num)
    sum, n1, n2 = 0, 1, 2

    # Loop through all while updating n1 and n2
    while n2 < num
      sum += n2 if n2.even?
      n1, n2 = n2, n1 + n2
    end

    sum

  end


  module ClassMethods
  end

end
