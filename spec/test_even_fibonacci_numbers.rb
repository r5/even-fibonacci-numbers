describe "EvenFibonacciNumbers" do
  describe "#fibonacci_sum" do
    it 'should return the correct sum when the limit is 10' do
      expect(VeryBigProject.new.fibonacci_sum(10)).to eq(10)
    end


    it 'should returns the correct sum when the limit is 4000000' do
      expect(VeryBigProject.new.fibonacci_sum(4000000)).to eq(4613732)
    end
  end
end
